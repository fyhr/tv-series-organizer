__author__ = 'Jesper Fyhr Knudsen'

import os
import shutil
import re


class MoveSeriesUseCase():
    def __init__(self, from_directory, to_directory):
        self.to_directory = to_directory
        self.from_directory = from_directory

    def move(self):
        for current_dir, sub_dirs, file_names in os.walk(self.from_directory):
            for f in file_names:

                series = self._find_series(f)

                if series is not None:
                    src = os.path.join(self.from_directory, f)
                    dst = os.path.join(self.to_directory, series, f)

                    shutil.move(src, dst)

                    print("Moving", src, "->", dst)

            for d in sub_dirs:
                series = self._find_series(d)

                if series is not None:
                    print("Moving", series)

                    for f in os.listdir(os.path.join(self.from_directory, d)):
                        src = os.path.join(self.from_directory, d, f)
                        dst = os.path.join(self.to_directory, series, f)

                        shutil.move(src, dst)

                        print("\t", src, "->", dst)

                    shutil.rmtree(os.path.join(current_dir, d))

            break

    def _find_series(self, candidate):
        for current_dir, sub_dirs, file_names in os.walk(self.to_directory):
            for series in sub_dirs:
                if re.search(series.replace("_", ".").replace(" ", "."), candidate, flags=re.IGNORECASE):
                    return series

            break

        return None
