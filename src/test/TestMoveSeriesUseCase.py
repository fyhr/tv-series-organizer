from src.MoveSeriesUseCase import MoveSeriesUseCase

__author__ = 'Jesper Fyhr Knudsen'

import unittest
import os
import shutil


class TestMoveSeriesUseCase(unittest.TestCase):
    def setUp(self):
        self.from_folder = "../../test_data/from"
        self.to_folder = "../../test_data/to"
        self.use_case = MoveSeriesUseCase(self.from_folder, self.to_folder)

        shutil.rmtree("../../test_data", ignore_errors=True)
        os.mkdir("../../test_data")
        os.mkdir(self.from_folder)
        os.mkdir(self.to_folder)
        os.mkdir(os.path.join(self.from_folder, "series.5"))
        open("../../test_data/from/series0", mode="w").close()
        open("../../test_data/from/series.1.long name", mode="w").close()
        open("../../test_data/from/prefix.series.2", mode="w").close()
        open("../../test_data/from/Something_series_3-after", mode="w").close()
        open("../../test_data/from/Something_series-4-after", mode="w").close()
        open("../../test_data/from/series.5/something.series_5.ext", mode="w").close()
        open("../../test_data/from/series.5/something.series_5.nfo", mode="w").close()

    def test_move_single_name_file(self):
        os.mkdir(self.to_folder + "/series0")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series0/series0"))

    def test_move_file_with_long_name(self):
        os.mkdir(self.to_folder + "/series.1")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series.1/series.1.long name"))

    def test_move_file_with_prefix_in_name(self):
        os.mkdir(self.to_folder + "/series.2")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series.2/prefix.series.2"))

    def test_move_file_different_seperator(self):
        os.mkdir(self.to_folder + "/series.3")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series.3/Something_series_3-after"))

    def test_move_where_to_folder_doesnt_use_dot(self):
        os.mkdir(self.to_folder + "/series_4")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series_4/Something_series-4-after"))

    def test_move_where_to_folder_doesnt_use_dot(self):
        os.mkdir(self.to_folder + "/series 5")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/series 5/something.series_5.ext"))
        self.assertFalse(os.path.exists(self.from_folder + "/series.5"), msg="from folder not removed")

    def test_move_single_name_file(self):
        os.mkdir(self.to_folder + "/Series0")

        self.use_case.move()

        self.assertTrue(os.path.exists(self.to_folder + "/Series0/series0"))